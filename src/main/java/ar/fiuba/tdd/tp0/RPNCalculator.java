package ar.fiuba.tdd.tp0;

import java.util.Scanner;
import java.util.Stack;

public class RPNCalculator {

    public float eval(String expression) {
        if (expression == null) {
            throw new IllegalArgumentException("Expression can not be null");
        }
        String[] ops = expression.split("\\s+");
        Stack<Float> stack = new Stack<Float>();
        Stack<Float> staux = new Stack<Float>();
        float arg2 = 0;
        float res = 0;
        for (String op: ops) {
            switch (op) {
                case "+":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("There are not 2 operands available for operation + ");
                    }
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("There are not 2 operands available for operation - ");
                    }
                    arg2 = stack.pop();
                    stack.push(stack.pop() - arg2);
                    break;
                case "*":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("There are not 2 operands available for operation * ");
                    }
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("There are not 2 operands available for operation / ");
                    }
                    arg2 = stack.pop();
                    if (arg2 == 0) {
                        throw new IllegalArgumentException("Zero division exception");
                    }
                    stack.push(stack.pop() / arg2);
                    break;
                case "MOD":
                    if (stack.size() < 2) {
                        throw new IllegalArgumentException("There are not 2 operands available for operation MOD ");
                    }
                    arg2 = stack.pop();
                    stack.push(stack.pop() % arg2);
                    break;
                case "++":
                    if (stack.size() < 1) {
                        throw new IllegalArgumentException("There is no operand available for operation ++ ");
                    }
                    res = 0;
                    while (!stack.isEmpty()) {
                        res += stack.pop();
                    }
                    stack.push(res);
                    break;
                case "--":
                    if (stack.size() < 1) {
                        throw new IllegalArgumentException("There is no operand available for operation -- ");
                    }
                    res = 0;
                    while (!stack.isEmpty()) {
                        arg2 = stack.pop();
                        res += -arg2;
                    }
                    res += 2 * arg2;                    
                    stack.push(res);
                    break;
                case "**":
                    if (stack.size() < 1) {
                        throw new IllegalArgumentException("There is no operand available for operation ** ");
                    }
                    res = 1;
                    while (!stack.isEmpty()) {
                        res *= stack.pop();
                    }
                    stack.push(res);
                    break;
                case "//":
                    if (stack.size() < 1) {
                        throw new IllegalArgumentException("There is no operand available for operation // ");
                    }
                    while (!stack.isEmpty()) {
                        staux.push(stack.pop());
                    }
                    res = staux.pop();
                    while (!staux.isEmpty()) {
                        arg2 = staux.pop();
                        if (arg2 == 0) {
                            throw new IllegalArgumentException("Zero division exception");
                        }
                        res /= arg2;
                    }
                    stack.push(res);
                    break;
                default:
                    int number = Integer.parseInt(op);
                    stack.push((float) number);
            }
        }
        return stack.pop();
    }
}
